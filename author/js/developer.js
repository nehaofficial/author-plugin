jQuery(document).ready(function($){

	$('#publish').click(function(){

		var first_name = $('#first_name').val();

		var last_name = $('#last_name').val();

		var facebook_url = $('#facebook_url').val();

		var google_plus_url = $('#google_plus_url').val();

		var linkedin_url = $('#linkedin_url').val();
		
		var biography = $('#biography').val();

		var urlregex = new RegExp("^(http:\/\/www.|https:\/\/www.|ftp:\/\/www.|www.){1}([0-9A-Za-z]+\.)");
		
		var error = 0;
		
		if(first_name == ""){
			$('#first_name_error').text('This field is required');
			error++;
		}else if(first_name !== ''){
			$('#first_name_error').text('');
		}

		if(last_name == ""){
			$('#last_name_error').text('This field is required');
			error++;
		}else if(last_name !== ''){
			$('#last_name_error').text('');
		}
		
		if(biography == ""){
			$('#biography_error').text('This field is required');
			error++;
		}else if(biography !== ''){
			$('#biography_error').text('');
		}
		
		if(facebook_url == ''){
			$('#facebook_url_error').text('');
			//return true;

		}else if(! facebook_url.match(urlregex)){
			$('#facebook_url_error').text('Kindly use valid url');
			error++;
		}else if(facebook_url.match(urlregex)){
			$('#facebook_url_error').text('');
		}

		if(google_plus_url == ''){
			$('#google_plus_url_error').text('');
			
		}else if(! google_plus_url.match(urlregex)){
			$('#google_plus_url_error').text('Kindly use valid url');
			error++;
		}else if(google_plus_url.match(urlregex)){
			$('#google_plus_url_error').text('');
		}

		if(linkedin_url == ''){
			$('#linkedin_url_error').text('');
			
		}else if(! linkedin_url.match(urlregex)){
			$('#linkedin_url_error').text('Kindly use valid url');
			error++;
		}else if(linkedin_url.match(urlregex)){
			$('#linkedin_url_error').text('');
		}
		
		if(error > 0){
			return false;
		}else{
			return true;
		}
		
	});

});
