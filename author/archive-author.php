<?php
/**
 * The template for displaying archive pages
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * If you'd like to further customize these archive views, you may create a
 * new template file for each one. For example, tag.php (Tag archives),
 * category.php (Category archives), author.php (Author archives), etc.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); 
?>

	<div class="content-section-a floatL">
		
        <div class="container col-lg-12">
			<?php if ( have_posts() ) : ?>
			
				<!-- Page Header Start-->
				<div class="row">
					<div class="col-lg-12">
						<h1 class="page-header">
							<small>
								<?php echo str_replace('Archives:','',the_archive_title()); ?>
							</small>
						</h1>
					</div>
				</div>
				<!-- Page Header End-->
				
				<!-- Authors Row Start-->
				<div class="row">
				
					<?php
					$i=1;
					$first = "First";
					$last = "Last";
					$biography="No biography added";
					
					// Start the Loop.
					while ( have_posts() ) : the_post();
					
					$post_meta = get_post_meta( get_the_ID());
					
					// Check if the custom field has a value.
					if ( ! empty( $post_meta ) ) {
						$first = ($post_meta['first_name'][0] !='')?$post_meta['first_name'][0]:"First";
						$last = ($post_meta['last_name'][0] !='')?$post_meta['last_name'][0]:"Last";
						$biography = ($post_meta['biography'][0] !='')?$post_meta['biography'][0]:"No biography added";
					}
					?>
									
						<div class="col-md-4 portfolio-item">
						
							<a href="<?php the_permalink(); ?>">
								<?php 
								if ( function_exists('has_post_thumbnail') && has_post_thumbnail() ) { ?>
									<?php //the_post_thumbnail(array(250, 250), array( 'class' => 'img-responsive' )); 
										$profile_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
										
									?> 
									<img class="img-responsive border5 archiveImg" src="<?php echo $profile_image; ?>" alt="">
								<?php }else{ ?>
									<img class="img-responsive border5 archiveImg" src="<?php echo plugins_url('img/default.jpg',__FILE__ ); ?>" alt="">
								<?php
								} 
								?>
							</a>
							
							<h3>
								<a href="<?php the_permalink(); ?>"><?php echo $first." ".$last; ?></a>
							</h3>
							
							<p><?php echo $biography; ?></p>
						
						</div>
						<?php 
						if($i%3==0){
							echo '</div><div class="row">';
						}
						?>
					
					<!-- Authors Row End-->
					
				<?php 
					$i++;
					endwhile;  
				?>
				</div>
			<?php
			else :
			?>
				<!-- Author Not Found Code Start-->
				<div class="row">
					<div class="col-lg-12">
						<h3>Authors not found.</h3>
					</div>
				</div>
				<!-- Author Not Found Code End-->
				
			<?php	
			endif;
			?>
		</div>
	</div>

<?php get_footer(); ?>
