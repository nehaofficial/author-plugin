<?php
/**
 * @package Author
 */
/*
Plugin Name: Author
Plugin URI: http://toptal.com/
Description: This plug-in provides an Custom Post Type (Author) over admin end by using this admin can add authors and get it linked with existing WordPress user along with that it displays the author listing and detail over front end. 
Version: 1.0
Author: Neha
Author URI: http://facebook.com/er.neha29
License: Later
Text Domain: Author
*/

/*
This plug-in provides an Custom Post Type (Author) over admin end by using this admin can add authors and get it linked with existing WordPress user along with that it displays the author listing and detail over front end.

Copyright 2016-2017 Automattic, Inc.
*/

	// Required file added from library folder
	require_once(plugin_dir_path(__FILE__).'library/enqueuing.php' );
	require_once(plugin_dir_path(__FILE__).'library/metabox.php' );
	require_once(plugin_dir_path(__FILE__).'library/ajax-update.php' );
	require_once(plugin_dir_path(__FILE__).'library/readmethods.php' );

	//Call the files required function through add_action
	add_action( 'add_meta_boxes', 'toptal_authors_register_metabox' );
	add_action( 'save_post', 'toptal_authors_save_perm_metadata', 1, 2 );
	add_action( 'admin_enqueue_scripts', 'toptal_authors_admin_enqueue_stuff' );
	add_action( 'wp_enqueue_scripts', 'toptal_authors_front_enqueue_stuff' , 100);
	add_action( 'wp_ajax_toptal_authors_update_temp', 'toptal_authors_update_temp' );
	add_action('add_meta_boxes_authors' , 'authors_post_binding_meta');
	add_action('publish_authors' , 'insert_meta_box_db');
	add_action( 'toptal_authors_post_types', 'add_image_galleries_on_author' );
	
	/*
	 * Function name : create_authors
	 * Functionality : This function is used to add custom post over admin end once plugin get actiavted
	*/

	//Hooking author post to theme setup
	add_action( 'init', 'create_authors' ); 
	function create_authors() {
		
		//Set Labels for Authors Post type
		$labels = array(
			'name'                => __( 'Authors', 'Post Type General Name'),
			'singular_name'       => __( 'Author', 'Post Type Singular Name'),
			'menu_name'           => __( 'Authors'),
			'parent_item_colon'   => __( 'Parent Author'),
			'all_items'           => __( 'All Authors'),
			'view_item'           => __( 'View Author'),
			'add_new_item'        => __( 'Add New Author'),
			'add_new'             => __( 'Add New'),
			'edit_item'           => __( 'Edit Author'),
			'update_item'         => __( 'Update Author'),
			'search_items'        => __( 'Search Author'),
			'not_found'           => __( 'Not Found'),
			'not_found_in_trash'  => __( 'Not found in Trash'),
			
		);
		
		// Set other options for Author Post Type
		$args = array(
			'label'               => __( 'authors'),
			'description'         => __( 'Author for toptal'),
			'labels'              => $labels,
			// Form Features which need to use in Authors Post Editor
			'supports'            => array( 'thumbnail', 'revisions' ),
			/* A hierarchical custom post type is like Pages and can have
			* Parent and child items. A non-hierarchical custom post type
			* is like Posts.
			*/	
			'hierarchical'        => false,
			'public'              => true,
			'show_ui'             => true,
			'show_in_menu'        => true,
			'show_in_nav_menus'   => true,
			'show_in_admin_bar'   => true,
			'menu_position'       => 7,
			'can_export'          => true,
			'has_archive'         => true,
			'exclude_from_search' => false,
			'publicly_queryable'  => true,
		);
		
		// Registering Authors Post
		register_post_type( 'authors', $args );
	}

	/*
		Function name : change_thumbnail_html
		Functionality : This function is used to change the set featured image text 
		to Author avatar text, that would easily understandable by user
	*/
	add_action('admin_head-post-new.php','change_thumbnail_html');
	add_action('admin_head-post.php','change_thumbnail_html');
	function change_thumbnail_html( $content ) {
		
		if ('authors' == $GLOBALS['post_type']){
			add_filter('admin_post_thumbnail_html','replace_set_featured_image_text');
		}

	}

	function replace_set_featured_image_text($content){
		
		$content = str_replace(__('Set featured image'), __('Set Author Avatar'), $content);
		$content = str_replace(__('Remove featured image'), __('Remove Author Avatar'), $content);
		return $content;
		
	}
	
	/*
		Function name : single_author_template
		Functionality : This function is used to overwrite the single.php for author single page
	*/
	add_filter('single_template', 'single_author_template');
	function single_author_template($single) {
		
		global $wp_query, $post;

		/* Checks for single template by post type */
		if ($post->post_type == "authors"){
			if(file_exists(plugin_dir_path( __FILE__ ) . '/single-author.php'))
				return plugin_dir_path( __FILE__ ) . '/single-author.php';
		}
		
		return $single;
	}
	
	/*
		Function name : archive_author_template
		Functionality : This function is used to overwrite the archive.php for author archive page
	*/
	add_filter( 'archive_template', 'archive_author_template' ) ;
	function archive_author_template( $archive_template ) {
		 global $post;

		 if ( is_post_type_archive ( 'authors' ) ) {
			  $archive_template = dirname( __FILE__ ) . '/archive-author.php';
		 }
		 return $archive_template;
	}