<?php
	function authors_post_binding_meta( $post ){
		add_meta_box('meta-box' , "Author's Personal Details" , 'add_new_authors_fields' , 'authors' , 'normal' , 'default');
	}
	
	function add_new_authors_fields(){

		$fname = get_post_meta(isset($_GET['post']) ? $_GET['post']: '' ,'first_name' , true);
		$lname = get_post_meta(isset($_GET['post']) ? $_GET['post']: '' ,'last_name' , true);
		$facebook_url = get_post_meta(isset($_GET['post']) ? $_GET['post']: '' ,'facebook_url' , true);
		$linkedin_url = get_post_meta(isset($_GET['post']) ? $_GET['post']: '' ,'linkedin_url' , true);
		$google_plus_url = get_post_meta(isset($_GET['post']) ? $_GET['post']: '' ,'google_plus_url' , true);
		$biography = get_post_meta(isset($_GET['post']) ? $_GET['post']: '' ,'biography' , true);
		$linked_wp_user = get_post_meta(isset($_GET['post']) ? $_GET['post']: '' ,'linked_wp_user' , true);
		
		$allWpUsers = get_users();
		
		?>
		<div class="container author_custom_fields">
			
			<div class="form-group">
				
				<label for="first_name">First Name<em class="required_fields">*</em></label>
				<input placeholder="First name" type="text" id="first_name" class="form-control" name="first_name" value="<?php echo @$fname ; ?>">
				<p class="error" id="first_name_error"></p>
			</div>
			
			<div class="form-group">
				
				<label for="last_name">Last Name<em class="required_fields">*</em></label>
				<input placeholder="Last name" type="text" id="last_name" class="form-control" name="last_name" value="<?php echo @$lname ; ?>">
				<p class="error" id="last_name_error"></p>
			</div>
			
			<div class="form-group">
				
				<label for="biography">Biography<em class="required_fields">*</em></label>
				<textarea id="biography" placeholder="Biography" class="form-control" name="biography"><?php echo @$biography ; ?></textarea>
				<p class="error" id="biography_error"></p>
			</div>
			
			<div class="form-group">
				
				<label for="facebook_url">Facebook profile url (Optional)</label>
				<input placeholder="Facebook profile url" type="text" id="facebook_url" class="form-control" name="facebook_url" value="<?php echo @$facebook_url ; ?>">
				<p class="error" id="facebook_url_error"></p>
			</div>
			
			<div class="form-group">
				
				<label for="google_plus_url">Google+ profile url (Optional)</label>
				<input placeholder="Google+ url" type="text" id="google_plus_url" class="form-control" name="google_plus_url" value="<?php echo @$google_plus_url ; ?>">
				<p class="error" id="google_plus_url_error"></p>
			</div>
			
			<div class="form-group">
				
				<label for="linkedin_url">Linkedin profile url (Optional)</label>
				<input placeholder="LInkedin profile url" type="text" id="linkedin_url" class="form-control" name="linkedin_url" value="<?php echo @$linkedin_url ; ?>">
				<p class="error" id="linkedin_url_error"></p>
			</div>
			
			<div class="form-group">
				
				<label for="linked_wp_user">Link Wordpress User</label>
				<select name="linked_wp_user" class="form-control" id="linked_wp_user">
					<?php
						foreach ( $allWpUsers as $user ) {
							echo '<option value="'.$user->ID.'" '.($linked_wp_user==$user->ID?'selected="selected"':'').'>' . esc_html($user->display_name) . '</option>';
						}
					?>	
				</select>
				
			</div>
			
		</div>	
		


	<?php	
	}
	
	function insert_meta_box_db($ID, $post){
		update_post_meta($ID, 'first_name', $_POST['first_name']);     
		update_post_meta($ID, 'last_name', $_POST['last_name']); 
		update_post_meta($ID, 'facebook_url', $_POST['facebook_url']);     
		update_post_meta($ID, 'linkedin_url', $_POST['linkedin_url']); 
		update_post_meta($ID, 'google_plus_url', $_POST['google_plus_url']);
		update_post_meta($ID, 'linked_wp_user', $_POST['linked_wp_user']);
		update_post_meta($ID, 'biography', $_POST['biography']);
	}
	
	function toptal_authors_register_metabox() {

		$post_types	= apply_filters( 'toptal_authors_post_types', array( 'post', 'page' ) );
		$context	= apply_filters( 'toptal_authors_context', 'side' );
		$priority	= apply_filters( 'toptal_authors_priority', 'default' );

		foreach ( $post_types as $post_type ) {

			add_meta_box( 'featuredgallerydiv', __( 'Author Image Gallery', 'featured-gallery' ), 'toptal_authors_display_metabox', $post_type, $context, $priority );

		}

	}

	function toptal_authors_display_metabox() {

		global $post;

		// Get the Information data if its already been added
		$galleryHTML = ''; $oldfix = '';
		
		$button = '<button class="media-modal-icon"></button>';
		$oldfix = ' premp6';
		
		$selectText    = __( 'Select Authors Images', 'featured-gallery' );
		$visible       = ''; //SHOULD WE SHOW THE REMOVE ALL BUTTON? THIS WILL BE APPLIED AS A CLASS, AND IS BLANK BY DEFAULT.
		$galleryArray  = get_post_gallery_ids( $post->ID );
		$galleryString = get_post_gallery_ids( $post->ID, 'string' );
		if ( ! empty( $galleryString ) ) {
			foreach ( $galleryArray as &$id ) {
				$galleryHTML .= '<li>'.$button.'<img id="'.$id.'" src="'.wp_get_attachment_url( $id ).'"></li> ';
			}
			$selectText = __( 'Edit Gallery', 'featured-gallery' );
			$visible    = " visible";
		} 
		update_post_meta( $post->ID, 'toptal_authors_temp_metadata', $galleryString ); // Overwrite the temporary featured gallery data with the permanent data. This is a precaution in case someone clicks Preview Changes, then exits withing saving. ?>

		<input type="hidden" name="toptal_authors_temp_noncedata" id="toptal_authors_temp_noncedata" value="<?php echo wp_create_nonce( 'toptal_authors_temp_noncevalue' ); ?>" />

		<input type="hidden" name="toptal_authors_perm_noncedata" id="toptal_authors_perm_noncedata" value="<?php echo wp_create_nonce( plugin_basename( __FILE__ ) ); ?>" />

		<input type="hidden" name="toptal_authors_perm_metadata" id="toptal_authors_perm_metadata" value="<?php echo $galleryString; ?>" data-post_id="<?php echo $post->ID; ?>" />

		<button class="button" id="toptal_authors_select"><?php echo $selectText; ?></button>

		<button class="button<?php echo $visible.$oldfix; ?>" id="toptal_authors_removeall"><?php _e( 'Remove All', 'featured-gallery' ); ?></button>

		<ul><?php echo $galleryHTML; ?></ul>

		<div style="clear:both;"></div><?php

	}

	function toptal_authors_save_perm_metadata( $post_id, $post ) {
		global $wpdb;
		//Only run the call when updating a Featured Gallery.
		if ( empty( $_POST['toptal_authors_perm_noncedata'] ) ) {
			return;
		}
		// Noncename
		if ( ! wp_verify_nonce( $_POST['toptal_authors_perm_noncedata'], plugin_basename( __FILE__ ) ) ) {
			return;
		}
		// Verification of User
		if ( ! current_user_can( 'edit_post', $post->ID ) ) {
			return;
		}
		// OK, we're authenticated: we need to find and save the data
		$events_meta['toptal_authors_perm_metadata'] = $_POST['toptal_authors_perm_metadata'];
		// Add values of $events_meta as custom fields
		foreach ( $events_meta as $key => $value ) {
			if ( $post->post_type == 'revision' ) {
				return;
			}
			$value = implode( ',', (array)$value );
			if ( get_post_meta( $post->ID, $key, FALSE ) ) {
				update_post_meta( $post->ID, $key, $value );
			} else {
				add_post_meta( $post->ID, $key, $value );
			}
			if ( ! $value ) {
				delete_post_meta( $post->ID, $key );
			}
		}

		if ( get_post_type( $post->ID ) == 'authors' ) {
			
			
			$fname = get_post_meta($post->ID, 'first_name', true);
			$lname = get_post_meta($post->ID, 'last_name', true);
			$title = $fname.' '.$lname;
			$slug = strtolower($fname.'-'.$lname);
			
			$updateData = array( 'post_title' => $title);
			
			if(@$_POST['original_publish']=='Publish'){
			
				$post_type = 'authors';
				
				$extra_checks = 'WHERE ID NOT IN('.$post->ID.') AND post_type = "authors" AND post_name LIKE "%'.$slug.'%"';
				
				$tt = $wpdb->get_row("SELECT count(*) AS pCount
														FROM $wpdb->posts
														$extra_checks
													");
													
										
				if($tt->pCount>0){
					$slug = $slug.'-'.$tt->pCount;
				}
				$updateData = array_merge($updateData,array('post_name' => $slug ));
			}
			
			
			
			$where = array( 'ID' =>$post->ID );
			$wpdb->update( $wpdb->posts, $updateData, $where );

		}

	}
	
	function add_image_galleries_on_author( $post_types ) {
		return array( 'authors' );
	}
	