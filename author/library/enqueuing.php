<?php
	/*
	 * Function name : toptal_authors_enqueue_stuff
	 * Functionality : This function is add CSS and JS file on admin end when plugin get activated
	*/
	function toptal_authors_admin_enqueue_stuff() {

		wp_enqueue_media();

		wp_enqueue_script( 'toptal-authors-script-admin', plugin_dir_url(dirname(__FILE__)).'js/admin.js' );

		wp_localize_script( 'toptal-authors-script-admin', 'myAjax', array( 'ajaxurl' => admin_url( 'admin-ajax.php' )));

		wp_enqueue_style( 'toptal-authors-style-admin', plugin_dir_url(dirname(__FILE__)).'css/admin.css' );

		wp_register_script('custom_developer_validation', plugin_dir_url(dirname(__FILE__)).'js/developer.js');
		
		wp_enqueue_style('custom_developer_style', plugin_dir_url(dirname(__FILE__)).'css/developer.css');
		
		$show_sidebar = apply_filters( 'toptal_authors_show_sidebar', false );

		if ( !$show_sidebar ) { wp_enqueue_style( 'toptal-authors-style-admin-hidesidebar', plugin_dir_url(dirname(__FILE__)).'css/admin-hidesidebar.css' ); }

	}
	
	/*
	 * Function name : toptal_authors_front_enqueue_stuff
	 * Functionality : This function is add CSS and JS file on front end when plugin get activated
	*/
	function toptal_authors_front_enqueue_stuff() {
		
		wp_enqueue_style( 'googleFonts', 'https://fonts.googleapis.com/css?family=Lato:300,400,700,300italic,400italic,700italic');
		wp_enqueue_script( 'bootstrap_min_js', plugin_dir_url(dirname(__FILE__)).'js/bootstrap.min.js' );
		wp_enqueue_style('bootstrap_min_css', plugin_dir_url(dirname(__FILE__)).'css/bootstrap.min.css');
		wp_enqueue_style('landing_page_css', plugin_dir_url(dirname(__FILE__)).'css/landing-page.css');
		wp_enqueue_style('developer_css', plugin_dir_url(dirname(__FILE__)).'css/developer.css');
		wp_enqueue_style('font_awesome_css', plugin_dir_url(dirname(__FILE__)).'font-awesome/css/font-awesome.min.css');
		
	}
	