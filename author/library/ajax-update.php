<?php

function toptal_authors_update_temp() {

	if ( ! wp_verify_nonce( $_REQUEST['toptal_authors_temp_noncedata'], "toptal_authors_temp_noncevalue" ) ) {
		exit( "Try later, something is going wrong." );
	}
	if ( ! current_user_can( 'edit_post', $_REQUEST['toptal_authors_post_id'] ) ) {
		exit( "Might be, You don't appear to be logged in, something is going wrong here." );
	}

	$newValue = $_REQUEST['toptal_authors_temp_metadata'];
	$oldValue = get_post_meta( $_REQUEST['toptal_authors_post_id'], 'toptal_authors_temp_metadata', 1 );
	$response = "success";

	if ( $newValue != $oldValue ) {

		$success = update_post_meta( $_REQUEST['toptal_authors_post_id'], 'toptal_authors_temp_metadata', $newValue );

		if ( $success == false ) {
			$response = "error";
		}

	}

	echo json_encode( $response );

	die();

}