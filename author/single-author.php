<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

get_header(); 
?>
   <!-- Page Content -->
	<div class="content-section-a floatL">
		
		<div class="container col-lg-12">
		<?php
			$first = "First";
			$last = "Last";
			$biography="No biography added";
			$facebook_url="Facebook profile not added";
			$google_plus_url="Google+ profile not added";
			$linkedin_url="linkedin profile not added";
			$gallery_images_id=0;
			$linked_wp_use=0;
			
			// Start the Loop.
			while ( have_posts() ) : the_post();
			
			$post_meta = get_post_meta( get_the_ID());
			
			// Check if the custom field has a value.
			if ( ! empty( $post_meta ) ) {
				$first = ($post_meta['first_name'][0] !='')?$post_meta['first_name'][0]:"First";
				$last = ($post_meta['last_name'][0] !='')?$post_meta['last_name'][0]:"Last";
				$biography = ($post_meta['biography'][0] !='')?$post_meta['biography'][0]:"No biography added";
				$facebook_url = ($post_meta['facebook_url'][0] !='')?$post_meta['facebook_url'][0]:"Facebook profile not added";
				$google_plus_url = ($post_meta['google_plus_url'][0] !='')?$post_meta['google_plus_url'][0]:"Google+ profile not added";
				$linkedin_url = ($post_meta['linkedin_url'][0] !='')?$post_meta['linkedin_url'][0]:"Linkedin profile not added";
				$gallery_images_id = ($post_meta['toptal_authors_temp_metadata'][0] !='')?$post_meta['toptal_authors_temp_metadata'][0]:0;
				$linked_wp_use = ($post_meta['linked_wp_user'][0] !='')?$post_meta['linked_wp_user'][0]:0;
				
				$args = array(
					'author'        =>  $linked_wp_use,
					'orderby'       =>  'post_date',
					'order'         =>  'ASC',
					'posts_per_page' => -1
				);
				
				// get his posts 'ASC'
				$linked_user_posts = get_posts( $args );

			}
			
		?>
       

		<!-- Page Header -->
			<div class="row">
				<div class="col-lg-12">
					<h1 class="page-header"><?php echo $first." ".$last; ?></h1>
				</div>
			</div>
			<!-- /.row -->

        <!-- Portfolio Item Row -->
			<div class="row">

				<div class="col-md-8">
					<?php 
						if ( function_exists('has_post_thumbnail') && has_post_thumbnail() ) { ?>
							<?php 
								$profile_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
							?> 
						<img class="img-responsive border5 singleImg" src="<?php echo $profile_image; ?>" alt="">
						<?php }else{ ?>
						<img class="img-responsive border5 singleImg" src="<?php echo plugins_url('img/default.jpg',__FILE__ ); ?>" alt="">
						<?php
						} 
					?>
					<div class="clearfix">&nbsp;</div>
					<h3>Author's Biography</h3>
					<p><?php echo $biography; ?></p>
				</div>

				<div class="col-md-4">
					<h3>Author's Social Profile</h3>
					<div id="social-bar">
						<a class="fb col-lg-12 col-md-12 col-sm-12 col-xs-12" href="<?php echo $facebook_url; ?>">
							<i class="fa  fa-facebook"></i>
							<span>Facebook</span>
						</a>
						
						<a class="gp col-lg-12 col-md-12 col-sm-12 col-xs-12" href="<?php echo $google_plus_url; ?>">
							<i class="fa  fa-google-plus"></i>
							<span>Google+</span>
						</a>
						<a class="ld col-lg-12 col-md-12 col-sm-12 col-xs-12" href="<?php echo $linkedin_url; ?>">
							<i class="fa  fa-linkedin"></i>
							<span>Linkedin</span>
						</a>
					</div>
					
					<!--Gallery Crausal Start-->
					<div class="clearfix"></div>
					<?php 		
					$galleryImgArray = get_post_gallery_ids($post->ID);
					if(!empty($galleryImgArray)){
					?>
						<h3>Author's Gallery</h3>
						
						<div class="bs-example">
						
							<div id="myCarousel" class="carousel slide" data-ride="carousel">
							
								<!-- Wrapper for carousel items -->
								<div class="carousel-inner">
									<?php 	
										$i=1;
										foreach ($galleryImgArray as $id) {
											
											if($i==1){$active = "active";}else{$active = "";}
											
											$imagesize = wp_get_attachment_image_src( $id, 'thumb' );
											
											echo '<div class="item '.$active.'"><img class="img-responsive border5 galleryImg" src="'.wp_get_attachment_url( $id ).'"></div>';
										$i++;
										}
									?>

								</div>
								<!-- Carousel controls -->
								<a class="carousel-control left" href="#myCarousel" data-slide="prev">
									<span class="glyphicon glyphicon-chevron-left"></span>
								</a>
								<a class="carousel-control right" href="#myCarousel" data-slide="next">
									<span class="glyphicon glyphicon-chevron-right"></span>
								</a>

						</div>

					</div>
					<?php 
					}
					?>
					
							
					<!--Gallery Crausal End-->
				</div>

			</div>
        <!-- /.row -->
		<?php 	endwhile;  ?>
        <!-- Related Projects Row -->
        <div class="row">

            <div class="col-lg-12">
                <h3 class="page-header">Linked user's related posts</h3>
            </div>
			
			<?php 
				if(isset($linked_user_posts) && count($linked_user_posts)>0){ 
					foreach ( $linked_user_posts as $post ) : setup_postdata( $post );
					?>
						<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
							<a href="<?php the_permalink(); ?>">
								<?php 
								if ( function_exists('has_post_thumbnail') && has_post_thumbnail() ) { ?>
										<?php //the_post_thumbnail(array(250, 250), array( 'class' => 'img-responsive' )); 
										$related_post_image = wp_get_attachment_url( get_post_thumbnail_id($post->ID) );
										
									?> 
									<img class="img-responsive border5 relatedPostImg portfolio-item" src="<?php echo $related_post_image; ?>" alt="">
								<?php }else{ ?>
									<img class="img-responsive border5 relatedPostImg portfolio-item" src="<?php echo plugins_url('img/default.jpg',__FILE__ ); ?>" alt="">
								<?php
								} 
								?>
							</a>
							<h4>
								<a href="<?php the_permalink(); ?>"><?php the_title();; ?></a>
							</h4>
						</div>
					<?php
					endforeach;
					wp_reset_postdata();
			?>
			
			<?php }else{ ?>
				<!-- Linked User Post Not Found Code Start-->
				<div class="row">
					<div class="col-lg-12">
						<h3>Authors not found.</h3>
					</div>
				</div>
				<!-- Linked User Post Not Found Code End-->
			<?php } ?>
		
        </div>
        <!-- /.row -->

        </div>
    </div>

<?php get_footer(); ?>
